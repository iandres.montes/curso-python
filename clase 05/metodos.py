# cuales son los metodos
# cuando se realiza una peticion se puede utilizar uno de los siguientes metodos:
""""
GET: (READ) Solicita un documento al servidor. Se puede enviar parametros en la url.
ejemplo: http://domain.com/usuarios.html?limit=10&filter=1
=================================================================
POST: (CREATE) Manda datos al servidor para ser procesados
ejemplo:    POST http://domain.com/usuarios?limit=10&filter=1
            {
                "name":"Fulano"
                "lastname":"de tal"
            }
=================================================================
PUT: (UPDATE) Almacena el documento enviado en el cuerpo del mensaje
ejemplo:    PUT http://domain.com/usuarios?limit=10&filter=1
            {
                "name":"Fulano"
                "lastname":"de tal"
            }
=================================================================
DELETE: (DELETE) eliminar el documento referenciado en la url
ejemplo:    DELETE http://domain.com/usuarios?limit=10&filter=1
            {
                "name":"Fulano"
                "lastname":"de tal"
            }
========================para que utilizamos los metodos==========================

CRUD: significa:

CREATE
READ
UPDATE
DELETE

MVC MODELO VISTA Y CONTROLADOR

========================cabeceras=============================
Server: indica el tipo de servidor http empleado 
Age: indica el tiempo que ha estado almacenado el objeto (en segundos)
Cache-Control: lo usa el servidor para decirle al navegador que objetos cachear, durante cuanto tiempo, etc.
Content-Encoding: se indica el tipo de codificacion aplicado para la respuesta
Expires: indica la fecha y la hora a partir del cual la repsuesta http se considera obsoleta.(Usado para poder gestionar cache)
Location: especifica una nueva ubicacion en caso de redireccion 
set-cookie: modificar cookies
"""

