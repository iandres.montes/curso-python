from flask import render_template, make_response, request
from config.config import Resource, db
from models.user import user

class Home(Resource):
    def __init__(self):
        pass

    def get(self):
        return "Hola"
    
    def post(self):
        data = request.get_json()
        name = data['name']
        email = data['email']

        new_user = user(name=name, email=email)
        db.session.add(new_user)
        db.session.commit()

        print(new_user.name)
        return {
            "message":"Recibido"
        }
        #make_response(render_template('home.html'), 200, {"Content-Type":'text/html'})
    
    def delete(self): 
        data = request.get_json()
        print(data)
        return {
            "error":False
        }


    def put(self):
        data = request.get_json()
        print(data)
        return {
            "message":"Put funciona"
        }