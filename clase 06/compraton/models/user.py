from config.config import db

class user(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250))
    email = db.Column(db.String(150), unique=True)

    def __repr__(self):
        return f'<User {self.id} >'
