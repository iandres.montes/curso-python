from router.web import app
from flask_script import Manager
from models.user import db

manager = Manager(app)

@manager.command
def migrate():
    """Migracion de la base de datos"""
    db.create_all()



if __name__ == '__main__':
    manager.run()