from flask import Flask
from flask_restful import Resource, Api
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__, template_folder=f'{os.getcwd()}/resource/views/',static_folder= f"{os.getcwd()}/public/",static_url_path='')

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

app.debug = True
api = Api(app)