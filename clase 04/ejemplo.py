#declaramos una clase que va a contener las operaciones de mi calculadora
class operaciones:
    '''
    creamos una funcion de inicializacion para la clase
    '''
    def __init__(self, arg, a, b):
        self.a = a
        self.b = b
        self.arg = arg 

        self.resultado = self.start() # luego de que se establecen las variables, ejecuto la funcion de la operacion
    

    #es la funcion del switch case
    def start(self):
        _json = {
            "suma":self.suma,
            "resta":self.resta
        }

        return _json[self.arg](self.a,self.b)

    def suma(self,a,b):
        return a+b
    
    def resta(self, a, b):
        return a-b
    

# arg = input("Que operacion haras? (suma o resta): ")
# num1 = int(input("Primero numero: "))
# num2 = int(input("Segundo numero: "))

# dato = operaciones(arg,num1,num2)
# print(dato.resultado)