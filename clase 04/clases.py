class Estudiante:
    def __init__(self,name, lastname, curse):
        self.name = name
        self.lastname = lastname
        self.curse = curse

    @staticmethod
    def estatica(cls, num):
        print(num)

    def saludo(self):
        return (f"Hola, bienvenido al curso {self.curse}! {self.name} {self.lastname}")


lista = [
    ['fulano','de tal','python'],
    ['maria', 'velez', 'ruby'],
    ['roberto', 'ruiz', 'javascript'],
]

new_list = []
for item in lista:
    dato = Estudiante(
        name=item[0],
        lastname=item[1],
        curse=item[2]
    )
    print(dato.saludo())
    new_list.append(dato)

print()
print(new_list[0].saludo())