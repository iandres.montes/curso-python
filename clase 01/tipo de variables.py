numero = 50 #integer (int) o numero
num2 = 15.8 #float, numero flotante o double
num3 = 'curso de python' #string o cadena de texto
num4 = "curso de python" #string o cadena de texto
num5 = True #(True, False, 1, 0) bool o boleano

print(type(numero))
print(type(num2))
print(type(num3))
print(type(num4))
print(type(num4))
print(type(num5))