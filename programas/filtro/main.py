import json

obj = [
    {
        "objectId": "dNxSoNxStF",
        "course": {
            "__type": "Pointer",
            "className": "Course",
            "objectId": "zpxwhrHpdz"
        },
        "name": "Unidad I",
        "description": "Unidad I",
        "createdAt": "2021-04-28T02:31:52.634Z",
        "updatedAt": "2021-04-28T02:31:52.634Z",
        "__type": "Object",
        "className": "Unit"
    },
    {
        "objectId": "Eph1HxIlpv",
        "course": {
            "__type": "Pointer",
            "className": "Course",
            "objectId": "zpxwhrHpdz"
        },
        "name": "Unidad II",
        "description": "Unidad II",
        "createdAt": "2021-04-28T02:32:05.184Z",
        "updatedAt": "2021-04-28T02:32:05.184Z",
        "__type": "Object",
        "className": "Unit"
    },
    {
        
        "objectId": "Eph1HxIlpv",
        "course": {
            "__type": "Pointer",
            "className": "Course",
            "objectId": "zpxwhrHpdz"
        },
        "name": "Unidad II",
        "description": "Unidad II",
        "createdAt": "2021-04-28T02:32:05.184Z",
        "updatedAt": "2021-04-28T02:32:05.184Z",
        "__type": "Object",
        "className": "Unit"
        
    }
]

aux = []
new_obj = []
for item in obj:
    if item['objectId'] in aux:
        pass
    else:
        new_obj.append(item)
        aux.append(item['objectId'])

# print(aux)
print(json.dumps(new_obj, indent=4))

