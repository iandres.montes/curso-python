from controller.operaciones import operaciones

def validate(num):
    try:
        return True,int(num)
    except Exception as error:
        return False,None

try:    
    operador = input("Que operacion deseas hacer? ( + , - , * , /) >>> ")
    if operador in ['+', '-', '*', '/']:
        validateNum1 = validate(input("Ingresa el primer numero: "))
        validateNum2 = validate(input("Ingresa el segundo numero: "))
        if validateNum1[0] and validateNum2[0]:
            resultado = operaciones().start(operador,validateNum1[1],validateNum2[1])
            print("El resultado de tu operacion es: ",resultado)
        else:
            print(">>> El numero ingresado no es correcto")
    else:
        print(f">>> La opcion '{operador}' no existe en el menu")

except Exception as error:
    print(error)
