import os

def menu(value):
    def opcionA():
        return "Opcion 'A' seleccionada"
    
    def opcionB():
        return "Opcion 'B' seleccionada"

    def opcionC():
        return "Opcion 'C' seleccionada"
    
    obj = {
        "1":opcionA,
        "2":opcionB,
        "3":opcionC,
    }
    return obj[str(value)]()


def pedirNumero():
    correcto = False
    num = 0
    while (not correcto):
        try:
            num = int(input('ingresa un numero entero: '))
            correcto = True
        except ValueError as err:
            print(err)
            print('Error, introduce un numero entero')
    
    return num

salir = False
opcion = 0

while not salir:
    print("1, Opcion A")
    print("2, Opcion B")
    print("3, Opcion C")
    print("4, Salir")

    print("Elige una opcion")

    opcion = pedirNumero()

    if opcion == 4:
        salir = True
        break
    if opcion in [1,2,3]:
        val = menu(opcion)
        os.system("cls")
        print(val)
print("FIN")