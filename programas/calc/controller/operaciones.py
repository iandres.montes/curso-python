class operaciones:
    def __init__(self):
        """Inicializamos la clase y sus variables"""
        self.obj = {
            "+":self.suma,
            "-":self.resta,
            "*":self.miltiplicacion,
            "/":self.division
        }

        # self.start()
    
    def start(self,operador,num1, num2):
        """Esta funcion esta encargada de iniciar el menu y retornar el resultado"""
        return self.obj[operador](num1,num2)

    def suma(self,a,b):
        """Esta es la funcion de suma"""
        return(a + b)
    
    def resta(self,a,b):
        """Esta es la funcion de resta"""
        return (a - b)

    def miltiplicacion(self, a,b):
        """Esta es la funcion de miltiplicacion"""
        return(a * b)
    
    def division(self,a,b):
        """Esta es la funcion de division"""
        return(a / b)