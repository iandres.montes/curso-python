# programa de un noob para noobs
# imprimiendo la tabla de multiplicar
def tabla():
    # sentencia para comprobar que num2 no tenga mas de 5 digitos
    if type(num3) is float and len(str(num3)) > 5:
        print("No coloque mas de cuatro digitos")
    # sentencia para comprobar que num2 no tenga mas de 6 digitos contanto el punto tambien
    if type(num3) is int and len(str(num3)) > 4:
        print("No coloque mas de cuatro digitos")

    # comprobar que el num3 contenga un numero entero y que contenga digitos menor e igual a 6 digits
    if type(num3) is int and len(str(num3)) <= 4:
        print(f"\nTabla de multiplicar de: {num3}")
        print("---------------------------------")
        for i in range(0, 12):
            print(f"{num3} x {i} = {num3 * i}")
    # comprobar que el num3 contenga un numero entero y que contenga digitos menor e igual a 5 digitos contanto con la coma decimal
    if type(num3) is float and len(str(num3)) <= 5:
        print(f"\nTabla de multiplicar de: {num3}")
        print("---------------------------------")
        for i in range(0, 12):
            print(f"{num3} x {i} = {round((num3 * i),3)}")


while True:
    try:
        num = input("Escriba un número: ")
        num = num.replace(" ", ",")
        if num == "":
            print("No Deje esto en blanco")
            continue
        try:
            for num2 in num.split(','):
                if num2 == ' ':
                    print("No intente probar el programa, no agregue espacios en la lista ._.")
                    continue
                try:
                    try:
                        num3 = int(num2)
                    except:
                        num3 = float(num2)
                except:
                    print(f"\nError: \"{num2}\"\nSolucion: Solo colocar numeros enteros(del 1 al 999) o flotantes(del 1.0 al 999.9)")
                    continue
                tabla()
            break
        except KeyboardInterrupt:
            print("Programa interrumpido")
            break
    except KeyboardInterrupt:
        print("Programa interrumpido")
        break
    except:
        if type(num) is str:
            print("\"->SOLO<-\" escriba numeros enteros o flotantes")
            continue
