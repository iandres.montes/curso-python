from bs4 import BeautifulSoup

contenido = """
    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Página de prueba</title>
    </head>
    <body>
    <div id="main" class="full-width">
        <h1>El título de la página</h1>
        <p>Este es el primer qwe</p>
        <p>Este es el primer párrafo</p>
    </div>
    </body>
    </html>
"""

soup = BeautifulSoup(contenido, 'lxml')

div_main = soup.div # este es el div contenedor padre
print(div_main.attrs) # estos son los atributos del div

pp = soup.p # extraigo el parrafo
texto = pp.string # traigo la informacion sin las etiquetas
print(texto)