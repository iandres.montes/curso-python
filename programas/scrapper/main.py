import requests, json
from bs4 import BeautifulSoup

class scrapper:
    def __init__(self):
        pass

    def find(self, data, tag, query, position = 0):
        fin = data.find_all(tag, query)
        if fin:
            # print(fin)
            fin = fin[position]
            return fin.string
        else:
            return None

    def parse(self, data):
        _json = {
            "title": self.find(data,"h1",{"class":"ui-pdp-title"}),
            "price": self.find(data,"span",{"class":"price-tag-fraction"}, position=1),
            "discount": self.find(data,"span",{"class":"ui-pdp-price__second-line__label ui-pdp-color--GREEN ui-pdp-size--MEDIUM"}),
        }
        return _json

    def start(self,url):
        res = requests.get(url)
        soup = BeautifulSoup(res.text, 'lxml')
        return(self.parse(soup))

file = open("information.json", 'w')

with open('lista.txt','r') as lista:
    
    objs = []
    for line in lista:
        item = scrapper().start(url=line.strip())
        objs.append(item)

    file.writelines(json.dumps(objs, indent=4))
    file.close()
    lista.close()