from config import app, socketio, render_template, join_room, emit

@app.route('/')
def index():
    return render_template("index.html")


@socketio.on('join')
def join(json):
    room = json['room']
    join_room(room)
    emit("join_response", {"message":f"Usuario conectado a la sala '{room}'"}, room=room)

@socketio.on('message')
def message(json):
    emit("message_response", {"message":json['message']},room=json['room'])

@socketio.on('broadcast')
def broadcast(json):
    emit("broadcast_response", {"message":json['message']}, broadcast=True)

if __name__ == '__main__':
    socketio.run(app,debug=True)