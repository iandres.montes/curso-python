
# Sintaxis del bucle for
"""
for variable in elemento_iterable: # (lista, cadena de texto, range, etc.) 
    #cuerpo del bucle
    pass
"""

##ejemplo de for
# print("comienzo")
# items = [5]
# for i in items:
#     print("Hola")
# print()
# print("Final")


#utilizar la variable de control del bucle
# print("comienzo")
# items = [1,2,3,4,5]

# for i in items:
#     print( f"Ahora i vale: {i} y su cuadrado es: {i ** 2}" )

# print()
# print("Final")


########################################

# print("Comienzo")
# for i in ["Alba", "Benito", 27]:
#     print(f"Hola. Ahora i vale {i}")
# print("Final")


## en caso de ser posible siempre usar range
# items = ["pera","manzana","banana","fresa","mora"]

# for i in range(0,5):
#     print( items[i] )

########## json ###########

# obj = {
#     "nombre":"Fulano",
#     "apellido":"De tal"
# }
# print(obj)


######### bucles con while ##############

'''
while condicion:
    cuerpo del bucle
'''

# ejemplo #1 
retorno = [
    {
        "nombre":"Fulano",
        "curso":"Python"
    },
    {
        "nombre":"Pedro",
        "curso":"Javascript"
    },
    {
        "nombre":"Maria",
        "curso":"Ruby"
    },
    {
        "nombre":"Juan",
        "curso":"Python"
    }
    
]
alumno_expulsado = ["Juan","Andres"]

cantidad = len(retorno)
aux = 1
i = 0
run = True

while run:
    if aux > cantidad:
        run = False
    else:
        item = retorno[i]
        nombre = item['nombre']
        curso = item['curso']
        
        if nombre not in alumno_expulsado and curso in ["Python", "Javascript"]:
            print(f"Bienvenido! {nombre} al curso de {curso}")
        
        aux += 1
        i += 1

