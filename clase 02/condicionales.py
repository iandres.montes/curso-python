"""
if test:
    code
else:
    code
"""

# x = 0
# curso = "Curso de python"
# if x == 0:
#     if curso == "Curso de python":
#         print("El Curso de python")
# else:
#     print("X no es igual a 3")

# que son las expresiones booleanas compuestas
lista_alumnos = ["Fulano", "Juan", "Maria"]
lista_curso = ["Python", "Ruby", "PhP"]

name = ""
lastname = ""
curso = "Python"


alumno_expulsado = "Jorge"
#operadores
"""
and: verifica que todas las condiciones en una sentencia se cumplan
in: verifica que un valor este dentro de una lista
or: ejecuta el codigo del condicional en caso de que cualquiera de las dos sea correcta
not: verifica que un item no este en la lista 
"""
dato = True
if name or lastname and curso in lista_curso and alumno_expulsado not in lista_alumnos and dato is True:
    print("tu nombre es: {}".format(name))
    print("Tu apellido es: {}".format(lastname))
    print("Tu curso es: {}".format(curso))
else:
    print("No se cumplieron las condiciones")


#condicionar listas
lista = [1,2,True, "Pera"]

if lista:
    print("Tiene datos")
else:
    print("No se cumplieron las condiciones")
