
#hacer lista saltando numeros de dos en dos
# l1 = list(range(10, 20, 2))
# print(l1)

# hacer una lista inversa
# l2 = list(range(20, 10, -1))
# print(l2)

# como se utiliza append()
# l3 = [1,2,3,4,5,6,7,8,9,10]
# l3.append(True)
# print(l3)


#como utilizar un remove()
# l4 = [1,2,3,4,5,6,7,8,9,10, "pera"]
# l4.remove("pera")
# print(l4)

# como utilizar index()
# l5 = ["pera","manzana","python","javascript"]
# print(l5.index("python"))

# como utilizar un count
# l6 = ["pera", "pera","manzana","python","python","python","javascript"]
# print(l6.count("python"))

# como utilizar reverse()
# l7 = [1,2,3,4,5,6,7,8,9,10]
# l7.reverse()
# print(l7)